// Iteration 1
module.exports = {
    googleProjectID: 'reactpageagent-rflbil',
    dialogFlowSessionID: 'react-bot-session',
    dialogFlowSessionLanguageCode: 'en-US'
} 

// Iteration 2
// if (process.env.NODE_ENV === 'production') {
//     // we are in production - return the prod set of keys
//     module.exports = require('./prod');
// } else {
//     // we are in development - return the dev keys!!!
//     module.exports = require('./dev');
// }