const dialogflow = require('dialogflow');
const config = require('../config/keys');

 const sessionClient = new dialogflow.SessionsClient();

 const sessionPath = sessionClient.sessionPath(config.googleProjectID, config.dialogFlowSessionID);

 const chatbot = require('../chatbot/chatbot');

module.exports = app => {

    app.get('/', (req, res) => {
        res.send({'hello': 'Johnny2'})
    });
    
    // app.post('/api/df_text_query', (req, res) => {
    //     let responses = chatbot.textQuery(req.body.text, req.body.parameters);
    //     res.send(responses[0].queryResult);
    // });
    app.post('/api/df_text_query', (req, res) => {
        const request = {
            session: sessionPath,
            queryInput: {
                text: {
                    text: req.body.text,
                    languageCode: config.dialogFlowSessionLanguageCode
                }
            }
        };
        sessionClient
        .detectIntent(request)
        .then(responses => {
            console.log('Detected intent');
            const result = responses[0].queryResult;
            console.log(`  Query: ${result.queryText}`);
            console.log(`  Response: ${result.fulfillmentText}`);
            if (result.intent) {
                console.log(`  Intent: ${result.intent.displayName}`);
                res.send(result);
            } else {
                console.log(`  No intent matched.`);
                res.send("No intent matched.");
            }
        })
        .catch(err => {
            console.error('ERROR:', err);
        });
   });	    

}
