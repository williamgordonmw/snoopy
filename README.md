# 2nd Chatbot with Heroku

From DialogFlow - 

Create ReactPageAgent

Create snoopy Intent:  

Training Phrase: Where can I find Snoopy?

Responses: Snoopy is nowhere to be found.

From Local -

install node

node index.js

localhost:5001

git add .

heroku.com - install heroku cli

heroku -v

heroku login

heroku create

heroku rename gordonfrog1

https://gordonfrog1.herokuapp.com

https://git.heroku.com/gordonfrog1.git

git remote add heroku https://git.heroku.com/gordonfrog1.git

git push heroku master

heroku logs --tail

https://gordonfrog1.herokuapp.com/

npm install nodemon --save-dev

npm run backend

http://localhost:5001

click ReactPageAgent settings

click service accounts

create service account

dialogflowclient

create key - 175cec5d74cd95fa7bbf8010829b57d367927e5e

reactpageagent-rflbil-175cec5d74cd.json

export GOOGLE_APPLICATION_CREDENTIALS=~/Downloads/reactpageagent-rflbil-175cec5d74cd.json

export GOOGLE_APPLICATION_CREDENTIALS=/Users/williamgordon/Downloads/reactpageagent-rflbil-175cec5d74cd.json

npm run backend

From Postman - 

POST http://localhost:5001/api/df_text_query
{
	"text":"where is snoopy?"
}

Response:

{
    "fulfillmentMessages": [
        {
            "platform": "PLATFORM_UNSPECIFIED",
            "text": {
                "text": [
                    "Snoopy is nowhere to be found."
                ]
            },
            "message": "text"
        },
        {
            "platform": "PLATFORM_UNSPECIFIED",
            "text": {
                "text": [
                    "Frogs are green.. and random."
                ]
            },
            "message": "text"
        }
    ],
    "outputContexts": [],
    "queryText": "where is snoopy?",
    "speechRecognitionConfidence": 0,
    "action": "",
    "parameters": {
        "fields": {}
    },
    "allRequiredParamsPresent": true,
    "fulfillmentText": "Snoopy is nowhere to be found.",
    "webhookSource": "",
    "webhookPayload": null,
    "intent": {
        "inputContextNames": [],
        "events": [],
        "trainingPhrases": [],
        "outputContexts": [],
        "parameters": [],
        "messages": [],
        "defaultResponsePlatforms": [],
        "followupIntentInfo": [],
        "name": "projects/reactpageagent-rflbil/agent/intents/97ac1097-4eb0-4631-8019-93ff9f90610c",
        "displayName": "snoopy",
        "priority": 0,
        "isFallback": false,
        "webhookState": "WEBHOOK_STATE_UNSPECIFIED",
        "action": "",
        "resetContexts": false,
        "rootFollowupIntentName": "",
        "parentFollowupIntentName": "",
        "mlDisabled": false
    },
    "intentDetectionConfidence": 1,
    "diagnosticInfo": null,
    "languageCode": "en"
}

