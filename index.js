//ITERATION 1
// const express = require('express');
// const bodyParser = require('body-parser');
// const app = express();

// app.get('/', (req, res) => {
//     res.send({'hello':'world'});
// });

// app.listen(5001);

//---

//ITERATION 2
// const express = require('express');
// const bodyParser = require('body-parser');
// const app = express();

// app.get('/', (req, res) => {
//     res.send({'hello':'world'});
// });

// const PORT = process.env.PORT || 5001;
// app.listen(PORT);

//---

//ITERATION 3
// const express = require('express');
// const bodyParser = require('body-parser');
// const app = express();

// app.get('/', (req, res) => {
//     res.send({'hello':'world'});
// });
// app.post('/api/df_text_query', (req, res) => {
//     res.send({'do':'text query'});
// });
// app.post('/api/df_event_query', (req, res) => {
//     res.send({'do':'event query'});
// });
// const PORT = process.env.PORT || 5001;
// app.listen(PORT);

//---

//ITERATION 4
const express = require('express');
const bodyParser = require('body-parser');
const app = express();

//  app.get('/', (req, res) => {	
//    app.use(bodyParser.json());
//    res.send({'hello': 'Johnny'})	
//  });	
 
 app.use(bodyParser.json());

 require('./routes/dialogFlowRoutes')(app);

 const PORT = process.env.PORT || 5001;
 app.listen(PORT);

//---

//FINAL
// const express = require('express');
// const bodyParser = require('body-parser');

// const app = express();

// const config = require('./config/keys');
// const mongoose = require('mongoose');
// mongoose.connect(config.mongoURI, { useNewUrlParser: true });

// require('./models/Registration');
// require('./models/Demand');
// require('./models/Coupons');


// app.use(bodyParser.json());

// require('./routes/dialogFlowRoutes')(app);
// require('./routes/fulfillmentRoutes')(app);

// if (process.env.NODE_ENV === 'production') {
//     app.use(express.static('client/build'));
//     const path = require('path');
//     app.get('*', (req, res) => {
//         res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
//     });
// }


// const PORT = process.env.PORT || 5000;
// app.listen(PORT);